import express, { Request, Response, NextFunction } from "express";
import mongoose from "mongoose";
import "express-async-errors";

import { router } from "./routes";

if (process.env.NODE_ENV !== 'production') {

  // Container database
  mongoose
      .connect(`mongodb://localhost:27017/test`, { useNewUrlParser: true, useUnifiedTopology: true })
      .then(() => {
          console.log('Connected to the Database successfully');
      });

}


const app = express();

app.use(express.json());

app.use(router);

app.listen(3000, () => {
  console.log("Server is running");
});