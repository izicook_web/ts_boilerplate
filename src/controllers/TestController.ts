import { Request, Response } from "express";

class TestController {

  async handle(request: Request, response: Response){
    return response.json('Test Controller is working');
  }

}

export { TestController }